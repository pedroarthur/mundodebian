FROM debian:testing-slim

RUN set -x; \
  : install gemfile dependencies && \
  apt-get update -yq && \
  apt-get install -yq --no-install-recommends \
    build-essential \
    libssl-dev \
    openssl \
    ruby-dev \
    ruby-bundler \
    zlib1g-dev \
    && \
  : clear the layer && \
  apt-get autoremove -yq && \
  apt-get clean -yq && \
  find /var/lib/apt/lists -type f | xargs -r rm && \
  find /tmp -type f -name '*.txt' | xargs -r rm && \
  : ;

COPY Gemfile      /tmp/jekyll/
COPY Gemfile.lock /tmp/jekyll/

RUN set -x; \
  cd /tmp/jekyll && \
  bundle install && \
  : ;

COPY . /srv/jekyll

# why we need this?
ENV JEKYLL_ENV docker

WORKDIR     /srv/jekyll
ENTRYPOINT ["/bin/bash", "-c"]
CMD        ["jekyll server --host=0.0.0.0 --watch --incremental"]
